"use client";

import React from "react";
import axios from "axios";
import { useAuth } from "@/auth/provider";
import { useMutation } from "@tanstack/react-query";
import TaskEdit from "./task-edit";
import parseDate from "date-fns/parse";
import isPast from "date-fns/isPast";
import formatDate from "date-fns/format";
import isEqual from "date-fns/isEqual";

interface TaskProps {
  id: number;
  title: string;
  description: string | null;
  due: string | null;
  todo: boolean;
  onAnything?: () => void; // called when editting or deleting an item
}

const Task: React.FC<TaskProps> = ({
  id,
  title,
  description,
  due,
  todo,
  onAnything,
}) => {
  const taskDate = parseDate(due || "0", "T", new Date());
  const date: Date | null = isEqual(parseDate("0", "T", new Date()), taskDate)
    ? null
    : taskDate;
  const isPastDue = date ? isPast(date) : false;

  const { token } = useAuth();
  const [isEditing, setIsEditing] = React.useState(false);

  const toggleEditing = () => setIsEditing(!isEditing);

  const toggleTask = async (): Promise<void> =>
    axios.put(
      "http://localhost:8000/task/" + id,
      { toggle: true },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );

  const deleteTask = async (): Promise<void> =>
    axios.delete("http://localhost:8000/task/" + id, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

  const mutateToggle = useMutation({
    mutationFn: toggleTask,
    onSuccess: onAnything,
  });

  const mutateDelete = useMutation({
    mutationFn: deleteTask,
    onSuccess: onAnything,
  });

  const handleMarkAsDone = () => {
    mutateToggle.mutate();
  };

  const handleDeleteTask = () => {
    mutateDelete.mutate();
  };

  if (isEditing)
    return (
      <TaskEdit
        id={id}
        title={title}
        description={description}
        dueDate={date ? formatDate(date, "yyyy-MM-dd") : null}
        onCancel={toggleEditing}
        onSubmit={() => {
          if (onAnything) onAnything();
          toggleEditing();
        }}
      />
    );

  return (
    <div className="p-4 border border-neutral-300 rounded-md">
      <h1
        className={`text-2xl font-bold mb-2 ${
          !todo ? "" : "line-through text-neutral"
        }`}
      >
        {title}
      </h1>
      <p className="text-base mb-2">{description}</p>
      <p
        className={`text-base font-semibold ${
          isPastDue ? "text-error" : "text-success"
        }`}
      >
        Due: {date ? formatDate(date, "yyyy-MM-dd") : "-"}
      </p>
      <div className="flex mt-3 space-x-2">
        <button
          onClick={handleMarkAsDone}
          className={!todo ? "btn btn-primary" : "btn btn-neutral"}
        >
          {!todo ? "Mark as Done" : "Mark as To do"}
        </button>
        <div className="flex-grow"></div>
        <button onClick={handleDeleteTask} className="btn btn-square btn-error">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
            />
          </svg>
        </button>

        <button className="btn btn-square btn-neutral" onClick={toggleEditing}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
            />
          </svg>
        </button>
      </div>
    </div>
  );
};

export default Task;
