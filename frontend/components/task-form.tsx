"use client";

import { useState } from "react";
import { useAuth } from "@/auth/provider";
import axios from "axios";
import { useMutation } from "@tanstack/react-query";

const TaskForm = ({ onCreate }: { onCreate?: () => void }) => {
  const { token } = useAuth();
  const createTask = async () =>
    axios.post(
      "http://localhost:8000/task",
      {
        title: task.title,
        description: task.description,
        due: task.dueDate,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );

  const mutation = useMutation({
    mutationFn: createTask,
    onSuccess: () => {
      setTask({
        title: "",
        description: "",
        dueDate: "",
      });
      if (onCreate) onCreate();
    },
  });

  const [task, setTask] = useState({
    title: "",
    description: "",
    dueDate: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setTask((prevTask) => ({
      ...prevTask,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    mutation.mutate();
  };

  return (
    <div className="p-4 max-w-md mx-auto md:max-w-md">
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <label
            htmlFor="title"
            className="text-sm font-semibold text-gray-600"
          >
            Title
          </label>
          <input
            type="text"
            id="title"
            name="title"
            value={task.title}
            onChange={handleChange}
            placeholder="Task title"
            className="w-full mt-1 input input-bordered input-primary"
            required
            maxLength={120}
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="description"
            className="text-sm font-semibold text-gray-600"
          >
            Description
          </label>
          <textarea
            id="description"
            name="description"
            value={task.description}
            onChange={handleChange}
            placeholder="Add your task description"
            className="w-full mt-1 textarea textarea-bordered textarea-primary"
            rows={3}
            maxLength={4096}
          ></textarea>
        </div>
        <div className="mb-4">
          <label
            htmlFor="dueDate"
            className="text-sm font-semibold text-gray-600"
          >
            Due date
          </label>
          <input
            type="date"
            id="dueDate"
            name="dueDate"
            value={task.dueDate}
            onChange={handleChange}
            className="w-full mt-1 input input-bordered input-primary"
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Add task
        </button>
      </form>
    </div>
  );
};

export default TaskForm;
