import { useState } from "react";
import { useAuth } from "@/auth/provider";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";

interface TaskEditProps {
  id: number;
  title: string;
  description: string | null;
  dueDate: string | null;
  onCancel?: () => void;
  onSubmit?: () => void;
}

const TaskEdit = ({
  id,
  title,
  description,
  dueDate,
  onSubmit,
  onCancel,
}: TaskEditProps) => {
  const [task, setTask] = useState({
    title: title,
    description: description,
    dueDate: dueDate,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setTask((prevTask) => ({
      ...prevTask,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    mutation.mutate();
  };

  const { token } = useAuth();
  const editTask = async () =>
    axios.put(
      `http://localhost:8000/task/${id}`,
      {
        title: task.title,
        description: task.description,
        due: task.dueDate,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );

  const mutation = useMutation({
    mutationFn: editTask,
    onSuccess: () => {
      setTask({
        title: "",
        description: "",
        dueDate: null,
      });
      if (onSubmit) onSubmit();
    },
  });

  return (
    <div className="p-4 max-w-md mx-auto md:max-w-md">
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <label
            htmlFor="title"
            className="text-sm font-semibold text-gray-600"
          >
            Title
          </label>
          <input
            type="text"
            id="title"
            name="title"
            value={task.title}
            onChange={handleChange}
            placeholder="Task title"
            className="w-full mt-1 input input-bordered input-primary"
            required
            maxLength={120}
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="description"
            className="text-sm font-semibold text-gray-600"
          >
            Description
          </label>
          <textarea
            id="description"
            name="description"
            value={task.description ?? ""}
            onChange={handleChange}
            placeholder="Add your task description"
            className="w-full mt-1 textarea textarea-bordered textarea-primary"
            rows={3}
            maxLength={4096}
          ></textarea>
        </div>
        <div className="mb-4">
          <label
            htmlFor="dueDate"
            className="text-sm font-semibold text-gray-600"
          >
            Due date
          </label>
          <input
            type="date"
            id="dueDate"
            name="dueDate"
            value={task.dueDate ?? ""}
            onChange={handleChange}
            className="w-full mt-1 input input-bordered input-primary"
          />
        </div>
        <div className="flex">
          <button type="submit" className="btn btn-primary">
            Save
          </button>
          <div className="flex-grow"></div>
          <button className="btn btn-error" onClick={onCancel}>
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default TaskEdit;
