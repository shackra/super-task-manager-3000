"use client";

import TaskForm from "@/components/task-form";
import Task from "@/components/task-item";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useAuth } from "@/auth/provider";
import { redirect } from "next/navigation";
import Link from "next/link";

export default function Tasks() {
  const { isAuthenticated, token } = useAuth();

  if (!isAuthenticated) {
    redirect("/login");
  }

  const { isPending, data, refetch } = useQuery({
    queryKey: ["tasks"],
    queryFn: async (): Promise<{
      tasks: {
        id: number;
        title: string;
        description: string;
        due: string;
        done: boolean;
      }[];
    }> => {
      const response = await axios.get("http://localhost:8000/task", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    },
  });

  return (
    <>
      <div className="flex bg-accent p-2">
        <div className="my-auto font-bold">Super Task Manager 3000</div>
        <div className="flex-grow"></div>
        <Link href="/logout" className="btn">
          Log out
        </Link>
      </div>
      <div className="min-h-screen">
        <div className="flex flex-col items-center justify-start max-h-screen p-6 space-y-6">
          <div className="w-full bg-base-100 p-6 rounded-md shadow-md md:max-w-md">
            <TaskForm onCreate={() => refetch()} />
          </div>
          <div className="p-6 space-y-4 max-h-fit md:max-w-md w-full overflow-auto">
            {isPending ? (
              <span className="loading loading-ball loading-xs"></span>
            ) : (
              data?.tasks.map((value) => (
                <Task
                  key={value.id}
                  id={value.id}
                  title={value.title}
                  description={value.description}
                  due={value.due}
                  todo={value.done}
                  onAnything={() => refetch()}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </>
  );
}
