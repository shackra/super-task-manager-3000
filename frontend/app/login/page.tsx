"use client";

import { useAuth } from "@/auth/provider";
import { useMutation } from "@tanstack/react-query";
import React from "react";
import axios, { AxiosResponse } from "axios";
import { redirect } from "next/navigation";
import Link from "next/link";

export default function Login() {
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [errorFlag, setErrorFlag] = React.useState(false);
  const { login, isAuthenticated } = useAuth();

  const doLogin = (): Promise<AxiosResponse<{ token: string }>> =>
    axios.post("http://localhost:8000/login", { username, password });

  const { mutate, isPending } = useMutation({
    mutationFn: doLogin,
    onSuccess: (response) => {
      setErrorFlag(false);
      login(response.data.token);
      setPassword("");
    },
    onError: (error) => {
      console.error({ error: error });
      setErrorFlag(true);
    },
  });

  const onSubmit = (e: any) => {
    e.preventDefault();
    mutate();
  };

  if (isAuthenticated) {
    redirect("/tasks");
  }

  return (
    <div className="flex items-center justify-center min-h-screen">
      <div className="w-full p-6 bg-base-100 rounded-md shadow-md md:max-w-md">
        <h1 className="text-3xl font-semibold text-center text-accent mb-2">
          Super Task Manager 3000
        </h1>
        <form className="space-y-4" onSubmit={onSubmit}>
          <div>
            <label
              htmlFor="username"
              className={errorFlag ? "text-error" : "text-base"}
            >
              Username
            </label>
            <input
              id="username"
              type="text"
              placeholder="john_smith"
              className={`w-full input input-bordered ${
                errorFlag ? "bg-error" : "bg-neutral"
              }`}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <label
              htmlFor="password"
              className={errorFlag ? "text-error" : "text-base"}
            >
              Password
            </label>
            <input
              id="password"
              type="password"
              placeholder="Enter Password"
              className={`w-full input input-bordered ${
                errorFlag ? "bg-error" : "bg-neutral"
              }`}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <button className="btn btn-primary w-full" onClick={onSubmit}>
              Login{" "}
              {isPending ? (
                <span className="loading loading-dots loading-md"></span>
              ) : null}
            </button>
          </div>
          <div>
            <p className="w-full text-center mb-2">or</p>
            <Link href="/register" className="btn btn-ghost w-full">
              Register
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
