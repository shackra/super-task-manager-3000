"use client";

import { useAuth } from "@/auth/provider";
import { redirect } from "next/navigation";

export default () => {
  const { logout } = useAuth();

  logout();
  redirect("/login");
};
