"use client";

import { useMutation } from "@tanstack/react-query";
import React from "react";
import axios, { AxiosResponse } from "axios";
import { redirect } from "next/navigation";
import Link from "next/link";

export default function Login() {
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [errorFlag, setErrorFlag] = React.useState(false);
  const [isRegistered, setIsRegistered] = React.useState(false);

  const doRegister = (): Promise<AxiosResponse<{ id: number; name: string }>> =>
    axios.post("http://localhost:8000/register", { username, password });

  const { mutate, isPending } = useMutation({
    mutationFn: doRegister,
    onSuccess: (response) => {
      setErrorFlag(false);
      setPassword("");
      setIsRegistered(username === response.data.name);
    },
    onError: (error) => {
      console.error({ error: error });
      setErrorFlag(true);
    },
  });

  const onSubmit = (e: any) => {
    e.preventDefault();
    mutate();
  };

  if (isRegistered) {
    redirect("/login");
  }

  return (
    <div className="flex items-center justify-center min-h-screen">
      <div className="w-full p-6 bg-base-100 rounded-md shadow-md md:max-w-md">
        <h1 className="text-3xl font-semibold text-center text-accent mb-2">
          Super Task Manager 3000
        </h1>
        <form className="space-y-4" onSubmit={onSubmit}>
          <div>
            <label
              htmlFor="username"
              className={errorFlag ? "text-error" : "text-base"}
            >
              Username
            </label>
            <input
              id="username"
              type="text"
              placeholder="john_smith"
              className={`w-full input input-bordered ${
                errorFlag ? "bg-error" : "bg-neutral"
              }`}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <label
              htmlFor="password"
              className={errorFlag ? "text-error" : "text-base"}
            >
              Password
            </label>
            <input
              id="password"
              type="password"
              placeholder="Enter Password"
              className={`w-full input input-bordered ${
                errorFlag ? "bg-error" : "bg-neutral"
              }`}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <button className="btn btn-primary w-full" onClick={onSubmit}>
              Register{" "}
              {isPending ? (
                <span className="loading loading-dots loading-md"></span>
              ) : null}
            </button>
          </div>
          <div>
            <p className="w-full text-center mb-2">or</p>
            <Link href="/login" className="btn btn-ghost w-full">
              Log in
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
