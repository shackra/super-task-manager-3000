"use client";

import React, {
  createContext,
  useState,
  useContext,
  FC,
  ReactNode,
  useEffect,
} from "react";

const TOKEN_KEY = "auth_key";

interface AuthContextProps {
  token: string | null;
  isAuthenticated: boolean;
  login: (userToken: string) => void;
  logout: () => void;
}

interface JWTData {
  exp: number;
}

const AuthContext = createContext<AuthContextProps | undefined>(undefined);

const CustomAuth: FC<{ children: ReactNode }> = ({ children }) => {
  const [token, setToken] = useState<string | null>(null);
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);

  const login = (userToken: string) => {
    setToken(userToken);
    localStorage.setItem(TOKEN_KEY, userToken);
    setIsAuthenticated(true);
  };

  const logout = () => {
    setToken(null);
    localStorage.removeItem(TOKEN_KEY);
    setIsAuthenticated(false);
  };

  const isTokenExpired = () => {
    const storedToken = localStorage.getItem(TOKEN_KEY);
    if (storedToken) {
      const parsedToken: JWTData = JSON.parse(atob(storedToken.split(".")[1]));
      const tokenExpiration = parsedToken.exp * 1000;
      return Date.now() > tokenExpiration;
    }
    return true;
  };

  useEffect(() => {
    if (isTokenExpired()) {
      setIsAuthenticated(false);
      setToken(null);
    } else {
      setIsAuthenticated(true);
      setToken(localStorage.getItem(TOKEN_KEY));
    }
  }, []);

  const contextValue: AuthContextProps = {
    token,
    isAuthenticated,
    login,
    logout,
  };

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

const useAuth = (): AuthContextProps => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be use inside a AuthContext provider");
  }
  return context;
};

export { CustomAuth, useAuth };
