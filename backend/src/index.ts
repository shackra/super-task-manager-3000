import express, { Application, Request, Response } from "express";
import { PrismaClient } from "@prisma/client";
import bodyParser from "body-parser";
import cors from "cors";
import HTTP from "./transport/http";
import AuthCRUD from "./crud/auth";
import AuthService from "./services/auth";
import TaskService from "./services/tasks";
import TaskCRUD from "./crud/tasks";
import { authenticateTokenWrapper } from "./middlewares/token";
import {
  validateCreateUser,
  validateCreateTask,
  validateEditTask,
  validateLoginUser,
} from "./middlewares/validation";

const secret = "secret-123";
const app: Application = express();

app.use(cors({ origin: true }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (_req: Request, res: Response) => {
  res.send("Healthy");
});

// setup the middleware
const authMiddleware = authenticateTokenWrapper(secret);

const prismaClient = new PrismaClient();
const authService = new AuthService(new AuthCRUD(prismaClient), secret);
const taskService = new TaskService(new TaskCRUD(prismaClient));

const http = new HTTP(authService, taskService);

app.post("/register", validateCreateUser, http.createUser);
app.post("/login", validateLoginUser, http.login);

app.get("/task", authMiddleware, http.listTasks);
app.delete("/task/:id", authMiddleware, http.deleteTask);
app.put("/task/:id", authMiddleware, validateEditTask, http.editTask);
app.post("/task", authMiddleware, validateCreateTask, http.createTask);

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});
