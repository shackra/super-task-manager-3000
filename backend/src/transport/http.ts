import TaskService from "../services/tasks";
import AuthService from "../services/auth";
import { Request, Response } from "express";
import { parse } from "date-fns";

interface RequestWithUser extends Request {
  user?: number;
}

class HTTP {
  private tasks: TaskService;
  private auth: AuthService;

  constructor(auth: AuthService, tasks: TaskService) {
    this.tasks = tasks;
    this.auth = auth;

    this.login = this.login.bind(this);
    this.createUser = this.createUser.bind(this);

    this.createTask = this.createTask.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    this.listTasks = this.listTasks.bind(this);
    this.editTask = this.editTask.bind(this);
  }

  // user creation and authentication related methods

  login(req: Request, res: Response) {
    let username = "";
    let password = "";

    if (req.body && req.body.username) {
      username = req.body.username;
    } else {
      res.status(400).send({ message: "username is empty" });
      return;
    }
    if (req.body && req.body.password) {
      password = req.body.password;
    } else {
      res.status(400).send({ message: "password is empty" });
      return;
    }

    this.auth
      .login(username, password)
      .then((result) => res.status(200).send({ token: result }))
      .catch((reason) => res.status(401).send({ message: reason }));
  }

  createUser(req: Request, res: Response) {
    let username = "";
    let password = "";

    if (req.body && req.body.username) {
      username = req.body.username;
    } else {
      res.status(400).send({ message: "username is empty" });
      return;
    }
    if (req.body && req.body.password) {
      password = req.body.password;
    } else {
      res.status(400).send({ message: "password is empty" });
      return;
    }

    this.auth
      .createUser(username, password)
      .then((result) => res.status(200).send({ id: result }))
      .catch((reason) =>
        res.status(409).send({ message: `cannot create user: ${reason}` }),
      );
  }

  // task related methods

  createTask(req: RequestWithUser, res: Response) {
    const owner = req.user;
    let title = "";
    let description = "";
    let due: Date;

    if (!owner) {
      res.status(400).send({ message: `unknown user ID: ${owner}` });
      return;
    }

    if (req.body && req.body.title) {
      title = req.body.title;
    } else {
      res.status(400).send({ message: "task title is empty" });
      return;
    }

    if (req.body && req.body.description) {
      description = req.body.description;
    }

    if (req.body && req.body.due) {
      due = new Date(parse(req.body.due, "yyyy-MM-dd", new Date()));
    }

    this.tasks
      .create(owner, title, description, due)
      .then((result) => res.status(200).send({ task: result }))
      .catch((reason) => res.status(500).send({ message: reason }));
  }

  editTask(req: Request, res: Response) {
    const taskID = parseInt(req.params.id);
    let toggleStatus = false;

    if (req.body && req.body.toggle !== undefined) {
      toggleStatus = req.body.toggle;
      if (toggleStatus) {
        this.tasks
          .toggle(taskID)
          .then((result) => res.status(200).send({ task: result }))
          .catch((reason) => res.status(500).send({ message: reason }));
        return;
      }
    }

    if (!toggleStatus) {
      let title = "";
      let description = "";
      let due: Date;

      if (req.body && req.body.title) {
        title = req.body.title;
      }

      if (req.body && req.body.description) {
        description = req.body.description;
      }

      if (req.body && req.body.due) {
        due = new Date(parse(req.body.due, "yyyy-MM-dd", new Date()));
      }

      this.tasks
        .edit(taskID, title, description, due)
        .then((result) => res.status(200).send({ task: result }))
        .catch((reason) => res.status(500).send({ message: reason }));
    }
  }

  deleteTask(req: Request, res: Response) {
    const taskID = parseInt(req.params.id);

    // FIXME: Any user can delete any task!!
    this.tasks
      .delete(taskID)
      .finally(() => res.status(200).send({ id: taskID }));
  }

  listTasks(req: RequestWithUser, res: Response) {
    const owner = req.user;

    if (owner === undefined) {
      res.sendStatus(401);
      return;
    }

    this.tasks
      .getAll(owner)
      .then((tasks) => res.status(200).send({ tasks }))
      .catch((reason) => res.status(500).send({ message: reason }));
  }
}

export default HTTP;
