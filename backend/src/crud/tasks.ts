import { PrismaClient, Task as TaskDB } from "@prisma/client";

class Task {
  private prisma: PrismaClient;

  constructor(client: PrismaClient) {
    this.prisma = client;
  }

  async createTask(
    ownerId: number,
    title: string,
    description: string,
    due: Date,
  ): Promise<TaskDB | never> {
    const task = await this.prisma.task.create({
      data: {
        title,
        description,
        ownerId,
        done: false,
        due,
      },
    });

    return task;
  }

  async getAllTasks(ownerId: number): Promise<TaskDB[]> {
    const tasks = await this.prisma.task.findMany({
      where: { ownerId },
    });
    return tasks;
  }

  async getTask(id: number): Promise<TaskDB | never> {
    const task = await this.prisma.task.findFirstOrThrow({ where: { id } });

    return task;
  }

  async editTask(
    taskId: number,
    title?: string,
    description?: string,
    due?: Date,
  ): Promise<TaskDB | null> {
    const data: { [k: string]: string | Date } = {};

    if (title) {
      data["title"] = title;
    }

    if (description) {
      data["description"] = description;
    }

    if (due) {
      data["due"] = due;
    }

    const updatedTask = await this.prisma.task.update({
      where: { id: taskId },
      data,
    });

    return updatedTask;
  }

  async deleteTask(taskId: number): Promise<TaskDB | null> {
    const deletedTask = await this.prisma.task.delete({
      where: { id: taskId },
    });
    return deletedTask;
  }

  async markTaskAsDone(taskId: number): Promise<TaskDB | null> {
    const updatedTask = await this.prisma.task.update({
      where: { id: taskId },
      data: { done: true },
    });
    return updatedTask;
  }

  async markTaskAsTodo(taskId: number): Promise<TaskDB | null> {
    const updatedTask = await this.prisma.task.update({
      where: { id: taskId },
      data: { done: false },
    });
    return updatedTask;
  }
}

export default Task;
