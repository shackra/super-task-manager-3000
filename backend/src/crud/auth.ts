import { PrismaClient, User } from "@prisma/client";
import bcrypt from "bcrypt";

const rounds = 10;

class Auth {
  private prisma: PrismaClient;

  constructor(client: PrismaClient) {
    this.prisma = client;
  }

  async createUser(name: string, password: string): Promise<User | never> {
    let hash: string;
    try {
      const salt = bcrypt.genSaltSync(rounds);
      hash = bcrypt.hashSync(password, salt);
    } catch (e) {
      throw Error(`unable to hash new user password: ${e}`);
    }

    try {
      const newUser = this.prisma.user.create({
        data: {
          name,
          password: hash,
        },
      });

      return newUser;
    } catch (e) {
      throw Error(`unable to create new user: ${e}`);
    }
  }

  async getUserID(name: string): Promise<number | never> {
    try {
      const user = await this.prisma.user.findFirstOrThrow({ where: { name } });
      return user.id;
    } catch (e) {
      throw Error(`unable to find user ${name}: ${e}`);
    }
  }

  async login(name: string, password: string): Promise<boolean | never> {
    try {
      const user = await this.prisma.user.findFirstOrThrow({ where: { name } });
      return bcrypt.compareSync(password, user.password);
    } catch (e) {
      throw Error(`unable to find user ${name}: ${e}`);
    }
  }
}

export default Auth;
