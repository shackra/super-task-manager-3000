import AuthCRUD from "../crud/auth";
import jwt from "jsonwebtoken";

class AuthService {
  private auth: AuthCRUD;
  private secret: string;

  constructor(client: AuthCRUD, secret: string) {
    this.auth = client;
    this.secret = secret;
  }

  async login(name: string, password: string): Promise<string | never> {
    try {
      const match = await this.auth.login(name, password);
      if (match) {
        const userID = await this.auth.getUserID(name);

        return jwt.sign({ user: `${userID}` }, this.secret, {
          expiresIn: "8h",
        });
      }

      throw Error("invalid name/password");
    } catch (e) {
      throw Error(`unable to authenticate user ${name}, error: ${e}`);
    }
  }

  async createUser(name: string, password: string): Promise<number | never> {
    try {
      const user = await this.auth.createUser(name, password);

      return user.id;
    } catch (e) {
      throw Error(`unable to create new user ${name}, error: ${e}`);
    }
  }
}

export default AuthService;
