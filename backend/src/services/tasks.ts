import TaskCRUD from "../crud/tasks";
import { Task, convertTaskDBsToTasks, convertTaskDBtoTask } from "./types";

class TaskService {
  private tasks: TaskCRUD;

  constructor(client: TaskCRUD) {
    this.tasks = client;
  }

  async create(
    ownerId: number,
    title: string,
    description: string,
    due: Date,
  ): Promise<Task | never> {
    try {
      const result = await this.tasks.createTask(
        ownerId,
        title,
        description,
        due,
      );
      return convertTaskDBtoTask(result);
    } catch (e) {
      throw Error(`cannot create new task, error: ${e}`);
    }
  }

  async getAll(ownerId: number): Promise<Task[] | never> {
    try {
      const results = await this.tasks.getAllTasks(ownerId);
      return convertTaskDBsToTasks(results);
    } catch (e) {
      throw Error(`failed to list all tasks for user, error: ${e}`);
    }
  }

  async edit(
    id: number,
    title?: string,
    description?: string,
    due?: Date,
  ): Promise<Task | never> {
    try {
      const result = await this.tasks.editTask(id, title, description, due);
      if (result === null) {
        throw Error(`task with id ${id} not found`);
      }
      return convertTaskDBtoTask(result);
    } catch (e) {
      throw Error(`failed to update task, error: ${e}`);
    }
  }

  async delete(id: number): Promise<Partial<Task>> {
    try {
      await this.tasks.deleteTask(id);
    } catch (e) {
      console.warn(
        `something went wrong when deleting task ${id}, error: ${e}`,
      );
    }

    return { id };
  }

  async toggle(id: number): Promise<Task | never> {
    try {
      const taskFound = await this.tasks.getTask(id);

      if (taskFound.done) {
        await this.tasks.markTaskAsTodo(id);
      } else {
        await this.tasks.markTaskAsDone(id);
      }
      return convertTaskDBtoTask({ ...taskFound, done: !taskFound.done });
    } catch (e) {
      throw Error(`failed to update task to-do status, error: ${e}`);
    }
  }
}

export default TaskService;
