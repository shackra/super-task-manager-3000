import { Task as TaskDB } from "@prisma/client";
import { format } from "date-fns";

export interface Task {
  id: number;
  title: string;
  description: string | null;
  done: boolean;
  owner: number;
  due: string | null;
}

export const convertTaskDBtoTask = (task: TaskDB): Task => {
  return {
    id: task.id,
    title: task.title,
    description: task.description,
    done: task.done,
    owner: task.ownerId,
    due: task.due ? format(task.due, "T") : null,
  };
};

export const convertTaskDBsToTasks = (tasks: TaskDB[]): Task[] => {
  return tasks.map(convertTaskDBtoTask);
};
