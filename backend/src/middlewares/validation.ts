import { createUser, loginUser } from "../validator/user";
import { toggleTodoStatus, task } from "../validator/task";
import validator from "../validator/validator";
import { Request, Response, NextFunction } from "express";

export const validateCreateUser = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  await validator(req.body, createUser, {}, (err, status) => {
    if (!status) {
      res.status(412).send({
        success: false,
        message: "validation failed",
        data: err,
      });
    } else {
      next();
    }
  }).catch((err) => console.error({ err }));
};

export const validateLoginUser = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  await validator(req.body, loginUser, {}, (err, status) => {
    if (!status) {
      res.status(412).send({
        success: false,
        message: "validation failed",
        data: err,
      });
    } else {
      next();
    }
  }).catch((err) => console.error({ err }));
};

export const validateCreateTask = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  await validator(req.body, task, {}, (err, status) => {
    if (!status) {
      res.status(412).send({
        success: false,
        message: "validation failed",
        data: err,
      });
    } else {
      next();
    }
  }).catch((err) => console.error({ err }));
};

export const validateEditTask = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  await validator(req.body, task, {}, async (_err, status) => {
    if (!status) {
      // check if we are toggling the status of the task
      await validator(req.body, toggleTodoStatus, {}, (err, status) => {
        if (!status) {
          res.status(412).send({
            success: false,
            message: "validation failed",
            data: err,
          });
        } else {
          next();
        }
      }).catch((err) => console.error({ err }));
    } else {
      next();
    }
  }).catch((err) => console.error({ err }));
};
