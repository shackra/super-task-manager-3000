import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

interface RequestWithUser extends Request {
  user?: number;
}

export const authenticateTokenWrapper = (secret: string) => {
  return (req: RequestWithUser, res: Response, next: NextFunction) => {
    const token = req.headers["authorization"]?.replace(/^Bearer\s/i, "");

    if (token == null) {
      console.error("authorization error: no token found");
      return res.sendStatus(401);
    }

    jwt.verify(token, secret, (err, payload) => {
      if (err) {
        console.error(`authorization error: ${err}`);
        return res.sendStatus(403);
      }

      let user = "";
      if (typeof payload === "string") {
        user = payload;
      } else if (typeof payload === "object" && payload !== undefined) {
        user = payload["user"] || "";
      }

      if (user === "") {
        return res.sendStatus(401);
      }

      const userID = parseInt(user);
      if (isNaN(userID)) {
        return res.sendStatus(401);
      }

      req.user = userID;

      next();
    });
  };
};
