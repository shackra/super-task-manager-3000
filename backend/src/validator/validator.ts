import validatorjs from "validatorjs";

const validator = async (
  body: any,
  rules: validatorjs.Rules,
  customMessages: validatorjs.ErrorMessages,
  callback: (errors: validatorjs.Errors | null, valid: boolean) => void,
) => {
  const validation = new validatorjs(body, rules, customMessages);
  validation.passes(() => callback(null, true));
  validation.fails(() => callback(validation.errors, false));
};

export default validator;
