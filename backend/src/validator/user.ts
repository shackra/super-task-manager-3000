export const createUser = {
  username: "string|required",
  password: "required|min:4|max:120",
};

export const loginUser = {
  username: "string|required",
  password: "required|min:4|max:120",
};
