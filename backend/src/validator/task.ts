export const toggleTodoStatus = {
  toggle: "required|boolean",
};

export const task = {
  title: "required|string|max:120",
  description: "string|max:4096",
  due: "date",
};
